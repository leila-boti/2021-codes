drop table if exists reporting_cast.SNAP_ALERT_rollup_2020 ;
create table reporting_cast.SNAP_ALERT_rollup_2020 as
select a.customer_shipto_account_number,a.customer_shipto_name,a.customer_shipto_market_segment,a.customer_shipto_market_segment_desc 
from aim.aim_customer_shipto_dim a
join (SELECT a.customer_shipto_account_number, max(customer_shipto_key) key_max  
        FROM aim.aim_customer_shipto_dim a
        join (select shipto_customer_number FROM reporting_comops.smrplus_ghs_account_monthly A
		WHERE smr_product_group in ('SNAP') AND territory_type in ('TDV', 'TSV') AND TEAM = 'AWT'
	    group by 1) b on b.shipto_customer_number = a.CUSTOMER_SHIPTO_ACCOUNT_NUMBER
        group by 1) b 
     on b.customer_shipto_account_number = a.customer_shipto_account_number and 
	 b.key_max = a.customer_shipto_key
where a.customer_shipto_market_segment NOT IN (38, 51, 52, 53, 91, 93, 94, 95, 99)	 
group by 1,2,3,4;

drop table if exists reporting_cast.SNAP_ALERT_rollup_LOWER_int ;
create table reporting_cast.SNAP_ALERT_rollup_LOWER_int as
select a.* ,
case 
	when Revenue_Measurement_Period  = 0 and Revenue_Baseline_Period <> 0 then 'Lost'
	when Revenue_Measurement_Period  = 0 and Revenue_Baseline_Period = 0 then 'Lost a long time ago'
	when (Revenue_Measurement_Period <> 0) and (Revenue_Baseline_Period = 0)  and (Before_Revenue_Baseline_Period <> 0) then 'New (Winback)'
	when (Revenue_Measurement_Period <> 0) and (Revenue_Baseline_Period = 0 ) and (Before_Revenue_Baseline_Period =  0) then 'New (Conversion)'
	when (Revenue_Measurement_Period  <> 0) and (Revenue_Baseline_Period <> 0)  and (Revenue_Measurement_Period > Revenue_Baseline_Period)  then 'Existing (Expanding)'
	when (Revenue_Measurement_Period  <> 0) and (Revenue_Baseline_Period <> 0)  and (Revenue_Measurement_Period <= Revenue_Baseline_Period)  then 'Existing (Decelerating)'
END as "Customer_Segment",
rank() over(order by Dollar_Change DESC) as rank_row
FROM (SELECT A.*, Revenue_Measurement_Period - Revenue_Baseline_Period Dollar_Change,
coalesce(((Revenue_Measurement_Period/Revenue_Baseline_Period)-1),0) Percent_Change
FROM (SELECT concat(shipto_customer_number, ' - ', b.customer_shipto_name) as Facility_Patient,
geography,region,district,concat(salesrep_name,'-',territory_code) Rep_name
,concat(dm_name,' - ',district) RSM_Name,concat(rvp_name, ' - ',region ) "RSL_Name",
SHORT_NAME "Care_Settings",b.customer_shipto_market_segment_desc "Market_Segment",
sum(CASE 
  when (trunc(month_id,'mm') >trunc(add_months(latest_date,-3),'mm') ) and 
       (trunc(month_id,'mm')<= trunc(latest_date,'mm'))
  		then revenue else 0 end) Revenue_Measurement_Period,
sum(CASE 
  when (trunc(month_id,'mm') > trunc(add_months(trunc(latest_date,'mm'),-6),'mm')) and 
       (trunc(month_id,'mm') <= trunc(add_months(trunc(latest_date,'mm'),-3),'mm'))
  		then revenue else 0 end) Revenue_Baseline_Period,
 sum(CASE 
  when trunc(month_id,'mm') <= trunc(add_months(trunc(latest_date,'mm'),-7),'mm')
  		then revenue else 0 end) Before_Revenue_Baseline_Period 		
FROM reporting_comops.smrplus_ghs_account_monthly A
JOIN reporting_cast.SNAP_ALERT_rollup_2020 B ON B.customer_shipto_account_number = A.shipto_customer_number 
LEFT JOIN sand_comops.snap_caresetting_2020_ABRV C ON C.CUSTOMER_SHIPTO_MARKET_SEGMENT= B.customer_shipto_market_segment 
WHERE smr_product_group in ('SNAP') AND territory_type in ('TDV', 'TSV') AND TEAM = 'AWT'
group by 1,2,3,4,5,6,7,8,9 ) A )A
order by Dollar_Change DESC;

--drop table if exists reporting_cast.SNAP_ALTER_rollup_LOWER_int2 ;
--create table reporting_cast.SNAP_ALTER_rollup_LOWER_int2 as
--select a.* from reporting_cast.SNAP_ALTER_rollup_LOWER_int a,
---(select max(rank_row ) max_row from reporting_cast.SNAP_ALTER_rollup_LOWER_int) b
--where (rank_row >= (max_row - 25))	or rank_row < 27;

drop table if exists reporting_cast.SNAP_ALERT_Dashboard_a;
create table reporting_cast.SNAP_ALERT_Dashboard_a as
select a.* from (
select a.*,avg_baseline_rev ,avg_Dollar_Change,
case 
when Revenue_Baseline_Period >= avg_baseline_rev and percent_change <= avg_Dollar_Change then 'Y' else 'N' end Alert
from reporting_cast.SNAP_ALERT_rollup_LOWER_int a,
(select avg(percent_change) avg_Dollar_Change from reporting_cast.SNAP_ALERT_rollup_LOWER_int
where  Customer_Segment like '%Decelerating%') b,
(select avg(Revenue_Baseline_Period) avg_baseline_rev from reporting_cast.SNAP_ALERT_rollup_LOWER_int
where  Customer_Segment like '%Decelerating%') C
)a;

drop table if exists reporting_cast.SNAP_ALERT_Dashboard ;
--create table reporting_cast.SNAP_ALERT_Dashboard as
--select * from reporting_cast.SNAP_ALERT_Dashboard;
create table reporting_cast.SNAP_ALERT_Dashboard as
select 
a.facility_patient,	a.geography,	a.region,	a.district,	a.rep_name,	a.rsm_name,	a.care_settings,	a.market_segment
,	case when rnum = 1 then a.Revenue_Measurement_Period else 0 end Revenue_Measurement_Period
,	case when rnum = 1 then a.Revenue_Baseline_Period else 0 end Revenue_Baseline_Period
,	case when rnum = 1 then  a.before_Revenue_Baseline_Period else 0 end before_Revenue_Baseline_Period
,	case when rnum = 1 then a.Dollar_Change else 0 end Dollar_Change
,	case when rnum = 1 then a.percent_change else 0 end percent_change
,case when rnum = 1 then b.num_purchases else 0 end Number_of_Purchases
,case when rnum = 1 then b.FIRST_purchase end FIRST_purchase 
,case when rnum = 1 then b.last_purchase end last_purchase 
,case when rnum = 1 then b.last_postive_purchase end last_postive_purchase 
,case when rnum = 1 then b.days_between end days_between 
,case when rnum = 1 then b.avg_Months_between_purchases end avg_Months_between_purchases 
,case when rnum = 1 then b.Months_since_recent_purhase end Months_since_recent_purhase
,	a.customer_segment,	a.rank_row,	a.avg_baseline_rev,	a.avg_Dollar_Change,	a.Alert ,	a.month_id
,	a.manual_source,	a.data_source,	a.kar_revenue_trx_type,	a.transaction_type_desc,	a.market_category
,	a.shipto_customer_number,	a.shipto_customer_name,		a.territory_type,	concat(a.rvp_name, ' - ',a.region) RSL_Name,	a.dm_name,	a.territory_code
,	a.salesrep_name,	a.smr_product_group,	a.product_brand,	a.product_sub_brand,	a.product_sku
,	a.customer_shipto_zip_code,	a.customer_shipto_zip4,	a.mmc_flag,	a.tracings_flag,	a.days_in_month
,	a.working_days_in_month,	a.elapsed_days,	a.elapsed_working_days,	a.revenue,	a.flag,	a.latest_date
,	a.smr_rpt_prod_flag,	a.gpo_name,	a.network,	a.system,	a.team,	a.team_identifier,	a.data_type
,	a.smr_commission_flag,	a.rpt_month,	a.nsl_name,	a.customer_shipto_market_segment
,	a.customer_shipto_market_segment_desc,	a.short_name
from (
SELECT 
a.facility_patient,	a.geography,	a.region,	a.district,	a.rep_name,	a.rsm_name,	a.care_settings,	a.market_segment
,	a.Revenue_Measurement_Period,	a.Revenue_Baseline_Period,	a.before_Revenue_Baseline_Period,	a.Dollar_Change,	a.percent_change
,	a.customer_segment,	a.rank_row,	a.avg_baseline_rev,	a.avg_Dollar_Change,	a.Alert,	b.month_id
,	b.manual_source,	b.data_source,	b.kar_revenue_trx_type,	b.transaction_type_desc,	b.market_category
,	b.shipto_customer_number,	b.shipto_customer_name,		b.territory_type,	b.rvp_name,	b.dm_name,	b.territory_code
,	b.salesrep_name,	b.smr_product_group,	b.product_brand,	b.product_sub_brand,	b.product_sku
,	b.customer_shipto_zip_code,	b.customer_shipto_zip4,	b.mmc_flag,	b.tracings_flag,	b.days_in_month
,	b.working_days_in_month,	b.elapsed_days,	b.elapsed_working_days,	b.revenue,	b.flag,	b.latest_date
,	b.smr_rpt_prod_flag,	b.gpo_name,	b.network,	b.system,	b.team,	b.team_identifier,	b.data_type
,	b.smr_commission_flag,	b.rpt_month,	b.nsl_name,	b.customer_shipto_market_segment
,	b.customer_shipto_market_segment_desc,	b.short_name,
ROW_NUMBER() OVER (PARTITION BY a.facility_patient,a.geography,a.region,a.district,a.rep_name,a.rsm_name ORDER BY a.facility_patient,a.geography,a.region,a.district,a.rep_name,a.rsm_name DESC) AS rnum
FROM reporting_cast.SNAP_ALERT_Dashboard_a A
LEFT JOIN (SELECT 
concat(shipto_customer_number, ' - ', b.customer_shipto_name) as facility_patient,concat(salesrep_name,'-',territory_code ) Rep_name
,A.*,C.*		
FROM reporting_comops.smrplus_ghs_account_monthly A
JOIN reporting_cast.SNAP_ALERT_rollup_2020 B ON B.customer_shipto_account_number = A.shipto_customer_number 
LEFT JOIN sand_comops.snap_caresetting_2020_ABRV C ON C.CUSTOMER_SHIPTO_MARKET_SEGMENT= B.customer_shipto_market_segment 
WHERE smr_product_group in ('SNAP') AND territory_type in ('TDV', 'TSV') AND TEAM = 'AWT'
) B ON B.facility_patient = A.facility_patient AND B.REP_NAME = A.REP_NAME) a
left join (SELECT A.*, case when days_between = 0 or num_purchases = 0 or Month_AVG = 0
             then 0
		     else  round((days_between/num_purchases)/Month_AVG) end avg_Months_between_purchases 
		  ,case when (lAST_POSTIVE_PURCHASE<> '1900-01-01 00:00:00') and month_avg <> 0 
		     then 
	           MONTHS_BETWEEN(latest_date , lAST_POSTIVE_PURCHASE)
	         else 0 end Months_since_recent_purhase
	FROM (select a.*,case when (days_between + months_between(last_purchase, first_purchase)) = 0 
	      then 0 else days_between/months_between(last_purchase, first_purchase) end Month_AVG 
     FROM (select a.*
   ,case when DATEDIFF(last_purchase,First_purchase) <= 0 then 0 else DATEDIFF(last_purchase,First_purchase) end  days_between	
	FROM (select concat(shipto_customer_number, ' - ', b.customer_shipto_name) as Facility_Patient ,latest_date,
	count(distinct( case when revenue > 0 then month_id end)) num_purchases,
	min( case when revenue <> 0 then month_id else '3020-01-01 00:00:00' end)  First_purchase,
	max( case when revenue <> 0 then month_id else '1900-01-01 00:00:00' end)  last_purchase,
	max( case when revenue > 0 then month_id  else '1900-01-01 00:00:00'end)  last_Postive_purchase
	FROM reporting_comops.smrplus_ghs_account_monthly A
	JOIN reporting_cast.SNAP_ALERT_rollup_2020 B ON B.customer_shipto_account_number = A.shipto_customer_number 
		WHERE smr_product_group in ('SNAP') AND territory_type in ('TDV', 'TSV') AND TEAM = 'AWT'
		group by 1,2 order by 1) a)a)a) b on b.Facility_Patient = a.Facility_Patient;
--drop table if exists reporting_cast.snap_alert_dashboard ;
--create table reporting_cast.snap_alert_dashboard as;
--select distinct(latest_date)
--from reporting_cast.snap_alert_dashboard;
